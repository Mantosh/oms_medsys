//
//  OfficeTypeData.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/27/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class OfficeTypeData : JsonDecodable  {

    private (set) var officeId : String?
    private (set) var officeName : String?
    
    required init(json: JSON) {
        
        officeId = json ["OfficeId"].stringValue
        officeName = json ["OfficeName"].stringValue
    }

    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var officeList : [OfficeTypeData] = []
        
        if let officeData : [JSON] = json {
            
            for officeJson in officeData {
                
                let offdata = OfficeTypeData (json: officeJson)
                officeList.append(offdata)
            }
        }
        return officeList
    }

}
