//
//  Problem.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/7/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Problem: JsonDecodable {
    
    private (set) var code : String?
    private (set) var description : String?
    private (set) var drugForm : String?
    private (set) var onSetDate : String?
    private (set) var lastModifiedBy : String?
    private (set) var lastModifiedDate : String?
    
    
    required init(json: JSON) {
        
        code = json ["Code"].stringValue
        description = json ["Description"].stringValue
        drugForm = json ["DrugForm"].stringValue
        onSetDate = json ["OnSetDate"].stringValue
        lastModifiedBy = json ["LastModifiedBy"].stringValue
        lastModifiedDate = json ["LastModifiedDate"].stringValue
        
    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var problemList : [Problem] = []
        
        if let problemData : [JSON] = json {
            
            for problemJson in problemData {
                
                let problemdata = Problem (json: problemJson)
                problemList.append(problemdata)
            }
        }
        return problemList
    }

}



