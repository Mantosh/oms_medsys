//
//  Notification.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/7/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Notification: JsonDecodable {

    private (set) var refill : String?
    private (set) var note : String?
    private (set) var error : String?
    private (set) var share : String?
    private (set) var requestShare : String?
    private (set) var rxPending : String?
    
    private (set) var rxNeedSigning : String?
    private (set) var rxNeedDualSign : String?
    private (set) var rxNeedDualVerify : String?
    private (set) var rxOrdersPending : String?

    required init(json: JSON) {
        
        refill = json ["Refill"].stringValue
        note = json ["Note"].stringValue
        error = json ["Error"].stringValue
        share = json ["Share"].stringValue
        requestShare = json ["RequestShare"].stringValue
        rxPending = json ["RxPending"].stringValue
        
        rxNeedSigning = json ["RxNeedSigning"].stringValue
        rxNeedDualSign = json ["RxNeedDualSign"].stringValue
        rxNeedDualVerify = json ["RxNeedDualVerify"].stringValue
        rxOrdersPending = json ["RxOrdersPending"].stringValue
    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var notificationList : [Notification] = []
        
        if let notificationData : [JSON] = json {
            
            for notificationJson in notificationData {
                
                let notificationdata = Notification (json: notificationJson)
                notificationList.append(notificationdata)
            }
        }
        return notificationList
    }
}
