//
//  Allergy.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/7/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Allergy: JsonDecodable {

    private (set) var allergyName : String?
    private (set) var lastModifiedBy : String?
    private (set) var lastModifiedDate : String?
    private (set) var onSetDate : String?
    private (set) var reaction : String?

    required init(json: JSON) {
        
        allergyName = json ["AllergyName"].stringValue
        lastModifiedBy = json ["LastModifiedBy"].stringValue
        lastModifiedDate = json ["LastModifiedDate"].stringValue
        onSetDate = json ["OnSetDate"].stringValue
        reaction = json ["Reaction"].stringValue
    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var allergyList : [Allergy] = []
        
        if let allergyData : [JSON] = json {
            
            for allergyJson in allergyData {
                
                let allergydata = Allergy (json: allergyJson)
                allergyList.append(allergydata)
            }
        }
        return allergyList
    }
}
