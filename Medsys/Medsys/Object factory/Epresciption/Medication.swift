//
//  Medication.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/7/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Medication: JsonDecodable {

    private (set) var direction : String?
    private (set) var drugBrandName : String?
    private (set) var drugForm : String?
    private (set) var drugStrength : String?
    private (set) var lastModifiedBy : String?
    private (set) var lastModifiedDate : String?
    private (set) var medRcopiaId : String?

    
    required init(json: JSON) {
        
        direction = json ["Direction"].stringValue
        drugBrandName = json ["DrugBrandName"].stringValue
        drugForm = json ["DrugForm"].stringValue
        drugStrength = json ["DrugStrength"].stringValue
        lastModifiedBy = json ["LastModifiedBy"].stringValue
        
        lastModifiedDate = json ["LastModifiedDate"].stringValue
        medRcopiaId = json ["MedRcopiaId"].stringValue
        
    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var medicationList : [Medication] = []
        
        if let medicationData : [JSON] = json {
            
            for medicationJson in medicationData {
                
                let allergydata = Medication (json: medicationJson)
                medicationList.append(allergydata)
            }
        }
        return medicationList
    }
}
