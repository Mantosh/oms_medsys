//
//  Prescription.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/6/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Prescription: JsonDecodable {

    private (set) var id : String?
    private (set) var providerId : String?
    private (set) var supervisorId : String?
    private (set) var code : String?
    private (set) var description : String?

    private (set) var frequency : String?
    private (set) var dosage : String?
    private (set) var quantity : String?
    private (set) var instructions : String?
    private (set) var patientId : String?

    private (set) var shortDate : String?
    private (set) var addedBy : String?
    private (set) var editedBy : String?
    private (set) var tS_Added : String?
    private (set) var tS_Changed : String?

    private (set) var printStatus : String?
    private (set) var locked : String?
    

    
    required init(json: JSON) {
        
        id = json ["Id"].stringValue
        providerId = json ["ProviderId"].stringValue
        supervisorId = json ["SupervisorId"].stringValue
        code = json ["Code"].stringValue
        description = json ["Description"].stringValue

        frequency = json ["Frequency"].stringValue
        dosage = json ["Dosage"].stringValue
        quantity = json ["Quantity"].stringValue
        instructions = json ["Instructions"].stringValue
        patientId = json ["PatientId"].stringValue

        shortDate = json ["shortDate"].stringValue
        addedBy = json ["AddedBy"].stringValue
        editedBy = json ["EditedBy"].stringValue
        tS_Added = json ["TS_Added"].stringValue
        tS_Changed = json ["TS_Changed"].stringValue

        printStatus = json ["PrintStatus"].stringValue
        locked = json ["Locked"].stringValue
    
    }

    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var prescriptionList : [Prescription] = []
        
        if let presData : [JSON] = json {
            
            for presJson in presData {
                
                let presdata = Prescription (json: presJson)
                prescriptionList.append(presdata)
            }
        
        }
        
        
        return prescriptionList
    }
    
}
