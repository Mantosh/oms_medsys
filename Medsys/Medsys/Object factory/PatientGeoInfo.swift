//
//  PatientGeoInfo.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/24/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class PatientGeoInfo: NSObject {

    private (set) var patientId : String?
    private (set) var emergencyContactName : String?
    private (set) var emergencyContactPhone : String?
    private (set) var patientFirstName : String?
    private (set) var patientMiddleName : String?

    private (set) var patientLastName : String?
    private (set) var homePhone : String?
    private (set) var mobilePhone : String?
    private (set) var sex : String?
    private (set) var SSN : String?

    private (set) var dateOfBirth : String?
    private (set) var state : String?
    private (set) var city : String?
    private (set) var zipCode : String?
    private (set) var patientAddress : String?

    private (set) var referralFirstName : String?
    private (set) var referralMiddleName : String?
    private (set) var referralLastName : String?
    
    private (set) var gauranatorFirstName : String?
    private (set) var gauranatorLastName : String?
    private (set) var gauranatorMiddleName : String?

    init (json : JSON)
    {
        let jsondata = json ["Result"].json

        patientId = jsondata ["patientId"].stringValue
        emergencyContactName = jsondata ["EmergencyContactName"].stringValue
        emergencyContactPhone = jsondata ["EmergencyContactPhone"].stringValue
        patientFirstName = jsondata ["PatientFirstName"].stringValue
        patientMiddleName = jsondata ["PatientMiddleName"].stringValue
        patientLastName = jsondata ["PatientLastName"].stringValue
        homePhone = jsondata ["homePhone"].stringValue
        mobilePhone = jsondata ["MobilePhone"].stringValue
        sex = jsondata ["Sex"].stringValue
        SSN = jsondata ["SSN"].stringValue
        
        gauranatorFirstName = jsondata ["GauranatorFirstName"].stringValue
        gauranatorLastName = jsondata ["GauranatorLastName"].stringValue
        gauranatorMiddleName = jsondata ["GauranatorMiddleName"].stringValue
        
        if sex == "1"
        {
            sex = "Male"
        }else {
            sex = "Female"
        }

        dateOfBirth = jsondata ["DateOfBirth"].stringValue
        
        state = jsondata ["State"].stringValue
        city = jsondata ["City"].stringValue
        zipCode = jsondata ["ZipCode"].stringValue
        patientAddress = jsondata ["PatientAddress"].stringValue
        
        referralFirstName = jsondata ["ReferralFirstName"].stringValue
        referralMiddleName = jsondata ["ReferralMiddleName"].stringValue
        referralLastName = jsondata ["ReferralLastName"].stringValue
    }
    
//    static func ArrayFromJsonFactroy(_ json: JSON) -> JsonDecodable {
//        
//        print(json)
//        var patientGeoList : [PatientGeoInfo] = []
//        
//        if let historyData : JSON = json {
//            
//            for geoJson in historyData {
//                
//                let geoData = PatientGeoInfo (json: geoJson)
//                patientGeoList.append(geoData)
//            }
//        }
//        return patientGeoList as! JsonDecodable
//    }
}
