//
//  PatientHistoryInfo.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/24/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class PatientHistoryInfo: JsonDecodable {

    private (set) var scheduleId : String?
    private (set) var startTime : String?
    private (set) var endTime : String?
    private (set) var appointmentDate : String?
    private (set) var note : String?
    private (set) var confirmed : String?
    private (set) var reserved : String?
    private (set) var priority : String?
    private (set) var dblBooking : String?
    private (set) var lockOut : String?
    
    private (set) var noShow : String?
    private (set) var addedBy : String?
    private (set) var changedBy : String?
    private (set) var dateAdded : String?
    private (set) var dateChanged : String?
    private (set) var patientFirstName : String?
    private (set) var patientMiddleName : String?
    private (set) var patientLastName : String?
    private (set) var columnDescription : String?
    private (set) var columnColor : String?
    
    private (set) var officeId : String?
    private (set) var appointmentCode : String?
    private (set) var appointmentName : String?
    private (set) var patientId : String?

    required init(json : JSON) {
        
        scheduleId = json ["ScheduleId"].stringValue
        startTime = json ["StartTime"].stringValue
        endTime = json ["EndTime"].stringValue
        appointmentDate = json ["AppointmentDate"].stringValue
        note = json ["Note"].stringValue
        confirmed = json ["Confirmed"].stringValue
        reserved = json ["Reserved"].stringValue
        priority = json ["Priority"].stringValue
        dblBooking = json ["DblBooking"].stringValue
        lockOut = json ["LockOut"].stringValue

        noShow = json ["NoShow"].stringValue
        addedBy = json ["AddedBy"].stringValue
        changedBy = json ["ChangedBy"].stringValue
        dateAdded = json ["DateAdded"].stringValue
        dateChanged = json ["DateChanged"].stringValue
        patientFirstName = json ["PatientFirstName"].stringValue
        patientMiddleName = json ["PatientMiddleName"].stringValue
        patientLastName = json ["PatientLastName"].stringValue
        columnDescription = json ["ColumnDescription"].stringValue
        columnColor = json ["ColumnColor"].stringValue

        officeId = json ["officeId"].stringValue
        appointmentCode = json ["AppointmentCode"].stringValue
        appointmentName = json ["AppointmentName"].stringValue
        patientId = json ["patientId"].stringValue

    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var patientHistoryList : [PatientHistoryInfo] = []
        
        if let historyData : [JSON] = json {
            
            for hisJson in historyData {
                
                let hisdata = PatientHistoryInfo (json: hisJson)
                patientHistoryList.append(hisdata)
            }
        }
        return patientHistoryList
    }
    
}
