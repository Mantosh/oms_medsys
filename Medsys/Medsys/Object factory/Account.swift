//
//  Account.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/14/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class Account: NSObject {
    
    private (set) var userId : String?
    private (set) var nId : String?
    private (set) var userName : String?
    private (set) var tokenValue : String?
    private (set) var EncrypttokenValue : String?
    private (set) var active : String?
    private (set) var lastPasswdChange : String?
    private (set) var gender : String?
    private (set) var passwdChangeDuration : String?
    private (set) var passwdChangeDay : String?
    private (set) var deviceId : String?
    private (set) var ProviderId : String?
    private (set) var PracticeId : String?


     init(json : JSON) {
        
        userId = json ["userId"].stringValue
        nId = json ["nId"].stringValue
        userName = json ["userName"].stringValue
        tokenValue = json ["tokenValue"].stringValue
        EncrypttokenValue = json ["EncrypttokenValue"].stringValue
        active = json ["active"].stringValue
        lastPasswdChange = json ["lastPasswdChange"].stringValue
        gender = json ["gender"].stringValue
        passwdChangeDuration = json ["passwdChangeDuration"].stringValue
        passwdChangeDay = json ["passwdChangeDay"].stringValue

        passwdChangeDuration = json ["passwdChangeDuration"].stringValue
        deviceId = json ["deviceId"].stringValue
        ProviderId = json ["ProviderId"].stringValue
        PracticeId = json ["PracticeId"].stringValue

        super.init()
        save()
    }
    
    func save() -> Void {
        
        NSKeyedArchiver.setClassName("Account", for: Account.self)
        
        let objectData  = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(objectData, forKey: "MedsysAccountData")
        UserDefaults.standard.synchronize()
    }

}
