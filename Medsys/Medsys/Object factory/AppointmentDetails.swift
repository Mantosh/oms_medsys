//
//  AppointmentDetails.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/15/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class AppointmentDetails: JsonDecodable {
    
    private (set) var addedBy : String?
    private (set) var appointmentDate : String?
    private (set) var changedBy : String?
    private (set) var columnColor : String?
    private (set) var columnDescription : String?
    private (set) var confirmed : String?
    private (set) var dateAdded : String?
    private (set) var dateChanged : String?
    private (set) var dblBooking : String?
    private (set) var endTime : String?
    private (set) var lockOut : String?
    private (set) var noShow : String?
    private (set) var note : String?
    private (set) var patientFirstName : String?
    private (set) var patientLastName : String?
    private (set) var patientMiddleName : String?
    private (set) var priority : String?
    private (set) var reserved : String?
    private (set) var scheduleId : String?
    private (set) var startTime : String?
    
    private (set) var officeId : String?
    private (set) var patientId : String?
    
    private (set) var appointmentCode : String?
    private (set) var appointmentName : String?
    
    
    
    required init(json : JSON) {
        
        addedBy = json["AddedBy"].stringValue
        appointmentDate = json["AppointmentDate"].stringValue
        changedBy = json["ChangedBy"].stringValue
        columnColor = json["ColumnColor"].stringValue
        columnDescription = json["ColumnDescription"].stringValue
        confirmed = json["Confirmed"].stringValue
        dateAdded = json["DateAdded"].stringValue
        dateChanged = json["DateChanged"].stringValue
        dblBooking = json["DblBooking"].stringValue
        
        lockOut = json["LockOut"].stringValue
        noShow = json["NoShow"].stringValue
        note = json["Note"].stringValue
        patientFirstName = json["PatientFirstName"].stringValue
        patientLastName = json["PatientLastName"].stringValue
        patientMiddleName = json["PatientMiddleName"].stringValue
        priority = json["Priority"].stringValue
        reserved = json["Reserved"].stringValue
        scheduleId = json["ScheduleId"].stringValue
        officeId = json ["officeId"].stringValue
        patientId = json ["patientId"].stringValue
        
        appointmentName = json ["AppointmentName"].stringValue
        appointmentCode = json ["AppointmentCode"].stringValue
        
        startTime = json["StartTime"].stringValue
        endTime = json["EndTime"].stringValue


    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var appointmentList : [AppointmentDetails] = []
        
        if let appointment : [JSON] = json {
            
            for appointmentJson in appointment {
                
                let appoint = AppointmentDetails (json: appointmentJson)
                
                appointmentList.append(appoint)
            }
        
        }
        
        let sorted_appointment = appointmentList.sorted { (first , second) in first.appointmentDate! > second.appointmentDate!}
        
        return sorted_appointment
    }
    
}
