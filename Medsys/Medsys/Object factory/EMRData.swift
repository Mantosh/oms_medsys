//
//  EMRData.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import JASON

class EMRData : JsonDecodable {
    
    private (set) var encounterId : String?
    private (set) var encounterDate : String?
    private (set) var addedBy : String?
    private (set) var reasonToVisit : String?
    private (set) var isFinalized : String?
    private (set) var changedBy : String?
    private (set) var encounterSummary : String?

    required init(json: JSON) {
                
        encounterId = json ["EncounterId"].stringValue
        encounterDate = json ["EncounterDate"].stringValue
        addedBy = json ["AddedBy"].stringValue
        reasonToVisit = json ["ReasonToVisit"].stringValue
        isFinalized = json ["IsFinalized"].stringValue
        changedBy = json ["ChangedBy"].stringValue
        encounterSummary = json ["EncounterSummary"].stringValue
    }
    
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable] {
        
        var emrDataList : [EMRData] = []
        
        if let emrData : [JSON] = json {
            
            for emrJson in emrData {
                
                let emrdata = EMRData (json: emrJson)
                emrDataList.append(emrdata)
            }
        }
        return emrDataList
    }
}
