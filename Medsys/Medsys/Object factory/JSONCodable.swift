//
//  JSONCodable.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/14/18.
//  Copyright © 2018 mantosh. All rights reserved.
//
//

import Foundation
import JASON


/// The objject conforming to this protocol can be instantiated by a JSON object.
protocol JsonDecodable {
    
    init (json : JSON)
    
    /// Creates an array of objects of this type
    ///
    /// - Parameter json: The JSON object array
    /// - Returns: Array of objects conforming to this type.
    static func ArrayFromJsonFactroy(_ json: [JSON]?) -> [JsonDecodable]
}
