//
//  CalLogic.swift
//  CalendarLogic
//
//  Created by Lancy on 01/06/15.
//  Copyright (c) 2015 Lancy. All rights reserved.
//

import Foundation
import SwiftyUtils

class CalendarLogic: Hashable {
    
    var hashValue: Int {
        return baseDate.hashValue
    }
    
    // Mark: Public variables and methods.
    var baseDate: Date {
        didSet {
            calculateVisibleDays()
        }
    }
    
    lazy var dateFormatter = DateFormatter()
    
    var currentMonthAndYear: String {
        dateFormatter.dateFormat = "LLLL yyyy"
        return dateFormatter.string(from: baseDate)
    }

    var currentMonthDays: [CalendarDate]?
    var previousMonthVisibleDays: [CalendarDate]?
    var nextMonthVisibleDays: [CalendarDate]?
    
    init(date: Date) {
        baseDate = date.firstDayOfTheMonth
        calculateVisibleDays()
    }
    
    func retreatToPreviousMonth() {
        baseDate = baseDate.firstDayOfPreviousMonth
    }
    
    func advanceToNextMonth() {
        baseDate = baseDate.firstDayOfFollowingMonth
    }
    
    func moveToMonth(_ date: Date) {
        baseDate = date
    }
    
    func isVisible(_ date: Date) -> Bool {
        let internalDate = CalendarDate(date: date)
        if currentMonthDays!.contains(internalDate) {
            return true
        } else if (previousMonthVisibleDays!).contains(internalDate) {
            return true
        } else if (nextMonthVisibleDays!).contains(internalDate) {
            return true
        }
        return false
    }
    
    func containsDate(_ date: Date) -> Bool {
        let date = CalendarDate(date: date)
        let logicBaseDate = CalendarDate(date: baseDate)

        if (date.month == logicBaseDate.month) &&
            (date.year == logicBaseDate.year) {
            return true
        }
        
        return false
    }
    
    //Mark: Private methods.
    var numberOfDaysInPreviousPartialWeek: Int {
        return baseDate.weekDay - 1
    }
    
    var numberOfVisibleDaysforFollowingMonth: Int {
        // Traverse to the last day of the month.
        var parts = baseDate.monthDayAndYearComponents
        
        parts.day = baseDate.numberOfDaysInMonth
        
        // 7*6 = 42 :- 7 columns (7 days in a week) and 6 rows (max 6 weeks in a month)
        return 42 - (numberOfDaysInPreviousPartialWeek + baseDate.numberOfDaysInMonth)
    }
    
    var calculateCurrentMonthVisibleDays: [CalendarDate] {
        var dates = [CalendarDate]()
        let numberOfDaysInMonth = baseDate.numberOfDaysInMonth
        let component = baseDate.monthDayAndYearComponents
        
        for i in 1...numberOfDaysInMonth {
            
            dates.append(CalendarDate(day: i, month: component.month!, year: component.year!))
        }
        
        return dates
    }
    
    var calculatePreviousMonthVisibleDays: [CalendarDate] {
        var dates = [CalendarDate]()
        
        let date = baseDate.firstDayOfPreviousMonth
        let numberOfDaysInMonth = date.numberOfDaysInMonth
        
        let numberOfVisibleDays = numberOfDaysInPreviousPartialWeek
        let parts = date.monthDayAndYearComponents
        
        let daysIndex = numberOfDaysInMonth - (numberOfVisibleDays - 1)
        
        if daysIndex <= numberOfDaysInMonth {
            for index in daysIndex...numberOfDaysInMonth {
                dates.append(CalendarDate(day: index, month: parts.month!, year: parts.year!))
            }
        }
        
        return dates
    }

    var calculateFollowingMonthVisibleDays: [CalendarDate] {
        var dates = [CalendarDate]()
        
        let date = baseDate.firstDayOfFollowingMonth
        let numberOfDays = numberOfVisibleDaysforFollowingMonth
        let parts  = date.monthDayAndYearComponents
        
        for i in 1...numberOfDays {
            dates.append(CalendarDate(day: i, month: parts.month!, year: parts.year!))
        }
        return dates
    }
    
    func calculateVisibleDays() {
        currentMonthDays = calculateCurrentMonthVisibleDays
        previousMonthVisibleDays = calculatePreviousMonthVisibleDays
        nextMonthVisibleDays = calculateFollowingMonthVisibleDays
    }
}

func ==(lhs: CalendarLogic, rhs: CalendarLogic) -> Bool {
    return lhs.hashValue == rhs.hashValue
}

func <(lhs: CalendarLogic, rhs: CalendarLogic) -> Bool {
    return (lhs.baseDate.compare(rhs.baseDate) == .orderedAscending)
}

func >(lhs: CalendarLogic, rhs: CalendarLogic) -> Bool {
    return (lhs.baseDate.compare(rhs.baseDate) == .orderedDescending)
}
