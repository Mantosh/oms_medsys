//
//  Date.swift
//  CalendarLogic
//
//  Created by Lancy on 01/06/15.
//  Copyright (c) 2015 Lancy. All rights reserved.
//

import Foundation

class CalendarDate: CustomStringConvertible, Equatable {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    static func ==(lhs: CalendarDate, rhs: CalendarDate) -> Bool {
        return ((lhs.day == rhs.day) && (lhs.month == rhs.month) && (lhs.year == rhs.year))
    }

    var day: Int
    var month: Int
    var year: Int
    
    var isToday: Bool {
        let today = CalendarDate(date: Date())
        return isEqual(today) == .orderedSame
    }
    
    func isEqual(_ date: CalendarDate) -> ComparisonResult {
        let selfComposite = (year * 10000) + (month * 100) + day
        let otherComposite = (date.year * 10000) + (date.month * 100) + date.day
        
        if selfComposite < otherComposite {
            return .orderedAscending
        } else if selfComposite == otherComposite {
            return .orderedSame
        } else {
            return .orderedDescending
        }
    }
    
    init(day: Int, month: Int, year: Int) {
        self.day = day
        self.month = month
        self.year = year
    }
    
    init(date: Date) {
        let part = date.monthDayAndYearComponents
        
        self.day = part.day!
        self.month = part.month!
        self.year = part.year!
    }
    
    var nsdate: Date {
        return Date.date(day, month: month, year: year)
    }
    
    var description: String {
        return "\(day)-\(month)-\(year)"
    }
}
