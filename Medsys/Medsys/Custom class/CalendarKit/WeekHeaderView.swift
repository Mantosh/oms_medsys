//
//  WeekHeaderView.swift
//  Calendar
//
//  Created by Lancy on 02/06/15.
//  Copyright (c) 2015 Lancy. All rights reserved.
//

import UIKit

class WeekHeaderView: UICollectionReusableView {

    @IBOutlet var labels: [UILabel]!
    
    let formatter = DateFormatter()
    
    override func awakeFromNib() {
        
        let count = formatter.weekdaySymbols.count
        
        if labels.count == count {
            for i in 0 ..< count {
                let weekDayString = formatter.weekdaySymbols[i] 
                
                labels[i].text = weekDayString.substring(to: weekDayString.index(weekDayString.startIndex, offsetBy: 3)).uppercased()
            }
        }
    }
    
}
