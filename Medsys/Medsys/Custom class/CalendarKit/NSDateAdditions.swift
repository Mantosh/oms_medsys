//
//  Additions.swift
//  CalendarLogic
//
//  Created by Lancy on 01/06/15.
//  Copyright (c) 2015 Lancy. All rights reserved.
//

import Foundation

extension Date {
    
    static func date(_ day: Int, month: Int, year: Int) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let dayString = String(format: "%02d", day)
        let monthString = String(format: "%02d", month)
        let yearString = String(format: "%04d", year)
        
        return dateFormatter.date(from: dayString + "/" + monthString + "/" + yearString)!
    }
   
    var startOfDay: Date {
        var components = self.components
        
        components.hour = 0
        components.minute = 0
        components.second = 0
        
        return Calendar.current.date(from: components)!
    }
    
    var endOfTheDay: Date {
        var components = self.components
        
        components.hour = 23
        components.minute = 59
        components.second = 59
        
        return Calendar.current.date(from: components)!
    }

    var firstDayOfTheMonth: Date {
        let components = Calendar.current.dateComponents([.month, .year], from: self)
        return Calendar.current.date(from: components)!
    }

    func compareMonthYear (with date2 : Date) -> ComparisonResult {
        let calendar = Calendar.current
        var monthyearComponent = calendar.dateComponents([.month, .year], from: self)
        let firstDate = calendar.date(from: monthyearComponent)!
        monthyearComponent = calendar.dateComponents([.month, .year], from: date2)
        let secondDate = calendar.date(from: monthyearComponent)!
        return firstDate.compare(secondDate)
    }

    var firstDayOfPreviousMonth: Date {
        return firstDay(false)
    }
    
    var firstDayOfFollowingMonth: Date {
        return firstDay(true)
    }
    
    var monthDayAndYearComponents: DateComponents {
        let components: Set <Calendar.Component> = [.year, .month, .day]
        
        return Calendar.current.dateComponents(components, from: self)
    }
    
    var weekDay: Int {
        return components.weekday!
    }
    
    var numberOfDaysInMonth: Int {
        return Calendar.current.range(of: .day, in: .month, for: self)!.count
    }
    
    var day: Int {
        return components.day!
    }
    
    var month: Int {
        return components.month!
    }
    
    var year: Int {
        return components.year!
    }
    
    var minute: Int {
        return components.minute!
    }
    
    var second: Int {
        return components.second!
    }
    
    var hour: Int {
        return components.hour!
    }
    
    //MARK: Private variable and methods.
    static let calendarUnit = NSCalendar.Unit(rawValue: UInt.max)

    var components: DateComponents {
        let components = (Calendar.current as NSCalendar).components(Date.calendarUnit, from: self)
        return components
    }
    
    func firstDay(_ followingMonth: Bool) -> Date {
        var dateComponent = DateComponents()
        dateComponent.month = followingMonth ? 1: -1
        
        let date = (Calendar.current as NSCalendar).date(byAdding: dateComponent, to: self, options: NSCalendar.Options(rawValue: 0))
        return date!.firstDayOfTheMonth
    }
}
