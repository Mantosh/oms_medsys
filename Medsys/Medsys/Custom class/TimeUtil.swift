//
//  TimeUtil.swift
//   
//
//

import Foundation

class TimeUtil {
    
    static let Global = TimeUtil ()
    
    static let savedSearchFormatter = DateFormatter ()
    static let timeFormatter = DateFormatter ()
    
    private let omsTimeFormatter : DateFormatter
    private let omsfollowUpDateFormatter : DateFormatter
    private let weekMonthDayFormatter : DateFormatter
    
    private let dateFormatter : DateFormatter
    private var thisCalendar : Calendar
    
    private var thisYear : Int = 0
    private var thisDay : Int = 0
    private var thisMonth : Int = 0
    
    private var gmtTimeZone = TimeZone (secondsFromGMT: 0)
    private var localTimeZone = TimeZone.current
    
    private var usingLocalForFollowUp = false

    private let timeFormat = "hh:mm a"
    private let monthDayFormat = "MMM d"
    private let monthDayTimeFormat = "MMM d hh:mm a"
    private let monthDayYearFormat = "MMM dd yyyy"
    private let monthDayYearTimeFormat = "MMM dd yyyy hh:mm a"
    private let weekMonthDayFormat = "E MMM d"
    
    
    private init ()
    {

        omsTimeFormatter = DateFormatter ()
        omsfollowUpDateFormatter = DateFormatter ()
        weekMonthDayFormatter = DateFormatter ()
        dateFormatter = DateFormatter ()
        
        thisCalendar = Calendar.current
        gmtTimeZone = TimeZone (secondsFromGMT: 0)
        
        localTimeZone = TimeZone.current
        
        resetAll()
    }
    
    func resetAll () -> Void {
        omsTimeFormatter.dateFormat = timeFormat
        omsTimeFormatter.timeZone = localTimeZone
        
        omsfollowUpDateFormatter.dateFormat = monthDayYearFormat
        omsfollowUpDateFormatter.timeZone = localTimeZone
        
        weekMonthDayFormatter.dateFormat = weekMonthDayFormat
        weekMonthDayFormatter.timeZone = localTimeZone
        
        dateFormatter.dateFormat = "yyyy MM dd"
        dateFormatter.timeZone = localTimeZone
    }
    
    
    
    func monthTimeString (from date : Date?) -> String
    {
        if let date = date {

            let isToday = thisCalendar.isDateInToday(date)

            let year = date.year

            omsTimeFormatter.dateFormat = monthDayFormat
            return omsTimeFormatter.string(from: date)
        }
        return ""
    }
    
//    func monthFullTimeString (from date : Date?) -> String
//    {
//        if let date = date {
//
//            let isToday = thisCalendar.isDateInToday(date)
//
//            let year = date.year
//
//            let isCurrentYear = isToday || year == thisYear
//
//            omsTimeFormatter.dateFormat = isToday ? timeFormat : monthDayFormat
//            var dateString = omsTimeFormatter.string(from: date)
//
//            if isToday {
//                return dateString
//            }
//
//            dateString += date.daySuffix() + (isCurrentYear ? ", " : " \(year), ")
//            omsTimeFormatter.dateFormat = timeFormat
//
//            return dateString + omsTimeFormatter.string(from: date)
//        }
//        return ""
//    }

//    func weekMonthDayString (from date : Date?) -> String {
//        if let date = date {
//
//            let year = date.year
//
//            return weekMonthDayFormatter.string(from: date) + date.daySuffix() + (year == thisYear ? "" : " \(year)")
//
////            return weekMonthDayFormatter.string(from: date) + date.daySuffix()
//        }
//        return ""
//    }
    
    func isDateInToday (_ date : Date) -> Bool {
        return thisCalendar.isDateInToday(date)
    }
    
//    func followUpTimeString(from date : Date?) -> String {
//
//        if let date = date {
//            let timeString = omsTimeFormatter.string(from: date)
//
//            return timeString
//        }
//
//        return ""
//    }
    
    func followUpDayString(from date : Date?) -> String {
        
        if let date = date {
            let dayString = weekMonthDayFormatter.string(from: date)
            
            return dayString
        }
        
        return ""
    }
    
    func dateString(from date : Date?) -> String {
        
        if let date = date {
            
            let dateString = dateFormatter.string(from: date)
            
            return dateString
        }
        
        return ""
    }

}
