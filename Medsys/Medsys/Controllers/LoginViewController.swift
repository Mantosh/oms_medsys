//
//  LoginViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/13/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoginViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var passwordImg: UIImageView!
    
    var officeData : [JsonDecodable] = []
    
    var appDele =  (UIApplication.shared.delegate as! AppDelegate)
    
    @IBOutlet weak var logoImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let appColor = UIColor.init(hex: "0E2C59")

        userNameText.leftView = UIView (frame: CGRect (x: 0, y: 0, width: 35, height: 40))
        userNameText.leftViewMode = .always
        passwordText.leftView = UIView (frame: CGRect (x: 0, y: 0, width: 35, height: 40))
        passwordText.leftViewMode = .always
        
        userImg.tintColor = UIColor.white
        passwordImg.tintColor = UIColor.white
        logoImage.tintColor = UIColor.white
        
        loginButton.designLayer(cornerRadius: 5.0, borderColor: UIColor.white , borderWidth: 1, maskToBounds: true)
        
        userNameText.text = "mobileapp"
        passwordText.text = "Pass@123"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        
        let username = (userNameText.text ?? "").trimmingCharacters(in: .whitespaces)
        let password = (passwordText.text ?? "").trimmingCharacters(in: .whitespaces)
        let deviceType = "mobile"
        
//        self.goNextView()
//        return

        if username.count == 0
        {
            let alert = UIAlertController.init(title: "Message", message: "Please enter login credentials", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        if password.count == 0
        {
            let alert = UIAlertController.init(title: "Message", message: "Please enter login credentials", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        startAnimating(CGSize(width: 50, height:50), message: "", type: NVActivityIndicatorType.lineScalePulseOut)
        
        AccountManager.Global.login(userName: username, password: password, deviceType: deviceType) { (success, jsonData, responceData) in
            
            self.stopAnimating()
            
            if let json = jsonData
            {
                let token = json ["Login"]["tokenValue"].stringValue
                let practiceId = json ["Login"]["PracticeId"].stringValue
                let providerId = json ["Login"]["ProviderId"].stringValue
                let deviceId = json ["Login"]["DeviceId"].stringValue
                let userId = json ["Login"]["userName"].stringValue
                
                UserDefaults.standard.set(token, forKey: "tokenValue")
                UserDefaults.standard.set(practiceId, forKey: "practiceId")
                UserDefaults.standard.set(providerId, forKey: "providerId")
                UserDefaults.standard.set(deviceId, forKey: "deviceId")
                UserDefaults.standard.set(userId, forKey: "userId")

                UserDefaults.standard.synchronize()
                
                let officeJson = json ["Office"].jsonArray
                
                self.officeData = OfficeTypeData.ArrayFromJsonFactroy(officeJson) 
                
                print(self.officeData)
                
                self.goNextView()
            }
            else
            {
                let alert = UIAlertController.init(title: "Message", message: responceData, preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        userNameText.resignFirstResponder()
        passwordText.resignFirstResponder()
    }
    
    func goNextView() -> Void {
        self.appDele.goHomePage()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
