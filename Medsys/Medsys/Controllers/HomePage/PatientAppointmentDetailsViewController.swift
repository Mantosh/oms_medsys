//
//  PaitentAppointmentDetailsViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class PatientAppointmentDetailsViewController: UIViewController {

    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var appointmentDateLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var dateAddedLabel: UILabel!
    @IBOutlet weak var dateChangedLabel: UILabel!
    @IBOutlet weak var columnDescLabel: UILabel!
    @IBOutlet weak var appointmentCodelabel: UILabel!
    @IBOutlet weak var appointmentNameLabel: UILabel!
    
    @IBOutlet weak var changedByLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    
    var patientAppointmentDetails : AppointmentDetails!
    
    var patientId : String!
    var patientName : String!
    
    var stTime : String!
    var endTime : String!
    var dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let appColor = UIColor.init(hex: "0E2C59")
        navigationItem.title = "Patient Appointment Details"
        navigationController?.navigationBar.barTintColor = appColor
        navigationController?.navigationBar.barStyle = .black
        
        topView.addShadow(radius: 3.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let strStartTime = patientAppointmentDetails?.startTime
        let strEndTime = patientAppointmentDetails?.endTime
        
        if let sTime = strStartTime
        {
            let time = sTime
            if time == "0"
            {
                stTime = "00:00"
            }
            else if time.count == 2
            {
                stTime = "00\(time)"
                stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
            }
            else if time.count == 3
            {
                stTime = "0\(time)"
                stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
            }
            else
            {
                stTime = time
                stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
            }
        }
        
        if let eTime = strEndTime
        {
            let time = eTime
            if time == "0"
            {
                endTime = "00:00"
            }
            else if time.count == 2
            {
                endTime = "00\(time)"
                endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
            }
            else if time.count == 3
            {
                endTime = "0\(time)"
                endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
            }
            else
            {
                endTime = time
                endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
            }
        }
        
        let  sTime1 = self.changeTimeformet(time: stTime)
        let  eTime1 = self.changeTimeformet(time: endTime)

        startTimeLabel.text = sTime1
        endTimeLabel.text = eTime1
        appointmentDateLabel.text = patientAppointmentDetails?.appointmentDate
        noteLabel.text = patientAppointmentDetails?.note
        addedByLabel.text = patientAppointmentDetails?.addedBy
        dateAddedLabel.text = patientAppointmentDetails?.dateAdded
        dateChangedLabel.text = patientAppointmentDetails?.dateChanged
        appointmentCodelabel.text = patientAppointmentDetails?.appointmentCode
        appointmentNameLabel.text = patientAppointmentDetails?.appointmentName
        changedByLabel.text = patientAppointmentDetails.changedBy

        let fName = patientAppointmentDetails?.patientFirstName!
        let lName = patientAppointmentDetails?.patientLastName!
        let patientId = patientAppointmentDetails?.patientId!

        let ffName = fName!
        let llName = lName!
        let pId = patientId!
        
        patientNameLabel.text = "\(String(describing: ffName)) \(String(describing: llName))"
        patientIdLabel.text = "Patient id :\(pId)"
        
    }

    func changeTimeformet(time : String) -> String {
        
        dateFormatter.dateFormat = "H:mm"
        let date12 = dateFormatter.date(from: time)!
        
        dateFormatter.dateFormat = "h:mm a"
        let date22 = dateFormatter.string(from: date12)
        
        return String(date22)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        clearSwRevealViewControllerMenuOptions()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //PatientInfoCell
}

