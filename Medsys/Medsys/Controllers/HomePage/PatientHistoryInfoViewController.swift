//
//  PatientHistoryInfoViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class PatientHistoryInfoViewController: UIViewController {

    var patientHistoryInfoList : [PatientHistoryInfo] =  []
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var patientNameLabel: UILabel!
    
    @IBOutlet weak var patientIdLabel: UILabel!
    
    var patientAppointmentDetails : AppointmentDetails!

    
    var patientId : String!
    var patientName : String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        topView.addShadow(radius: 3.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)

        let appColor = UIColor.init(hex: "0E2C59")
        navigationItem.title = "Patient History Info"
        navigationController?.navigationBar.barTintColor = appColor
        navigationController?.navigationBar.barStyle = .black
        
        let fName = patientAppointmentDetails?.patientFirstName!
        let lName = patientAppointmentDetails?.patientLastName!
        let id = patientAppointmentDetails?.patientId!
        
        let ffName = fName!
        let llName = lName!
        patientId = id!
        
        patientNameLabel.text = "\(String(describing: ffName)) \(String(describing: llName))"
        patientIdLabel.text = "Patient id :\(patientId!)"
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 146

        //---
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self?.getPatientHistoryData()

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.tableView.dg_stopLoading()
            })
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(appColor)
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
        //---
        
        getPatientHistoryData()
    }
    
    deinit {
        tableView.dg_removePullToRefresh()
    }

    func getPatientHistoryData()
    {
        let tokenValue = UserDefaults.standard.value(forKey: "tokenValue")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        
        let requestType = "AppointmentHistory"
        
        AccountManager.Global.getPatientHistoryData(practiceId: practiceId as! String, patientId: patientId, requestType: requestType) { (success, jsonData, responceData) in
                        
            self.patientHistoryInfoList = jsonData as! [PatientHistoryInfo]
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        clearSwRevealViewControllerMenuOptions()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PatientHistoryInfoViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return patientHistoryInfoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyInfoCell") as! PatientHistoryInfoTableViewCell
        cell.selectionStyle = .none
        
         cell.history = patientHistoryInfoList [indexPath.row]
        
        
        return cell
    }
}


//extension PatientHistoryInfoViewController : UITableViewDelegate
//{
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 146
//    }
//}

