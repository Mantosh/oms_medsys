//
//  EMRWebViewViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/25/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class EMRWebViewViewController: UIViewController , UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var emrHtmldata : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        webView.loadHTMLString(emrHtmldata, baseURL: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        print("did started")
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false

    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        print("did get error")

    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true

        print("did finished loading")

    }

}
