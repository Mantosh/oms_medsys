//
//  PatientGeoInfoViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PatientGeoInfoViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    
    
    var geoInfoList : PatientGeoInfo?
    let datePicker = UIDatePicker()
    
    var patientId : String!
    var patientName : String!
    
    var patientAppointmentDetails : AppointmentDetails!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let appColor = UIColor.init(hex: "0E2C59")
        topView.addShadow(radius: 3.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)

        navigationItem.title = "Patient Geo Info"
        navigationController?.navigationBar.barTintColor = appColor
        navigationController?.navigationBar.barStyle = .black
   
        let fName = patientAppointmentDetails?.patientFirstName!
        let lName = patientAppointmentDetails?.patientLastName!
        let id = patientAppointmentDetails?.patientId!
        
        let ffName = fName!
        let llName = lName!
        patientId = id!
        
        patientNameLabel.text = "\(String(describing: ffName)) \(String(describing: llName))"
        patientIdLabel.text = "Patient id :\(patientId!)"


    }

    override func viewWillAppear(_ animated: Bool) {
        
        getPatientGeoInfoData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        clearSwRevealViewControllerMenuOptions()
    }
    
    
    func getPatientGeoInfoData()
    {
        let requestType = "PatientGeo"

        startAnimating(CGSize(width: 50, height:50), message: "", type: NVActivityIndicatorType.lineScalePulseOut)
        
        AccountManager.Global.getPatientGeoData(patientId: patientId, requestType: requestType) { (success, jsonData, responceData) in
            
            self.stopAnimating()
            print("jsonData- Geo --- \(String(describing: jsonData))")
            
            self.geoInfoList = jsonData
            
            self.tableView.reloadData()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PatientGeoInfoViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeoInfoCell") as! PatientGeoInfoTableViewCell
        cell.selectionStyle = .none
        
        cell.geoData =  geoInfoList
        
//        cell.delegate = self
        
        return cell
    }
}


extension PatientGeoInfoViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 900
    }
}


