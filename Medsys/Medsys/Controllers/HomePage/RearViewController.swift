//
//  RearViewController.swift
//  SwiftRevealViewController
//
//  Created by Kristijan Kontus on 23/09/2016.
//  Copyright © 2016 kkontus. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case View = 0
    case Photo
    case Map
    case Blah
    case Video
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class RearViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, LeftMenuProtocol {
    @IBOutlet weak var tableView: UITableView!
   
    var mainViewController: UIViewController!
    var ePrescriptionViewController: UIViewController!
    var mapViewController: UIViewController!
    var blahViewController: UIViewController!
    var videoViewController: UIViewController!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var userProfile: UIImageView!
    
    
    let menus = ["Home","E-prescription", "Doc & Images", "Alerts", "EMR" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.size.width - 100
        self.revealViewController().rearViewRevealOverdraw = 0
        self.revealViewController().rearViewRevealDisplacement = 0
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.mainViewController = UINavigationController(rootViewController: mainViewController)
        
        let ePrescriptionViewController = storyboard.instantiateViewController(withIdentifier: "EPrescriptionViewController") as! EPrescriptionViewController
        self.ePrescriptionViewController = UINavigationController(rootViewController: ePrescriptionViewController)
        
        let mapViewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        self.mapViewController = UINavigationController(rootViewController: mapViewController)
        
        let blahViewController = storyboard.instantiateViewController(withIdentifier: "BlahViewController") as! BlahViewController
        self.blahViewController = UINavigationController(rootViewController: blahViewController)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView(frame: CGRect.zero) //IMPORTANT: this hides empty cells (and cell separators)
        
        userProfile.designLayer(cornerRadius: userProfile.frame.width/2, borderColor: UIColor.white, borderWidth: 2.0, maskToBounds: true)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appColor = UIColor.init(hex: "0E2C59")

        bottomView.addShadow(radius: 2.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = menus[indexPath.row]
            
            switch menu {
            case .View:
                //you can add some cell costumisation here
                cell.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
                return cell
            case .Photo:
                //you can add some cell costumisation here
                cell.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
                return cell
            case .Map:
                //you can add some cell costumisation here
                cell.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
                return cell
            case .Blah:
                //you can add some cell costumisation here
                cell.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
                return cell
            case .Video:
                //you can add some cell costumisation here
                cell.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu: menu)
            
            switch menu {
            case .View: break
            case .Photo: break
            case .Map: break
            case .Blah: break
            case .Video: break
            }
        }
    }
    
    @IBAction func signOutTapped(_ sender: Any) {
        
        
//        UserDefaults.standard.removeAll()
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
//        viewCtr.patientAppointmentDetails = historyDetails
//
//        backItem.title = "Back"
//        navigationItem.backBarButtonItem = backItem
//        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationController?.pushViewController(viewCtr, animated: true)
        
        
    }
    /*
     * This way we instantiate ViewController when needed, not on SideMenu initialization (viewDidLoad), because we don't want it in memory until we go to that view
     */
    func instantiateDynamicallyCreatedViewControllers() {
        let videoViewController = VideoViewController()
        videoViewController.view.backgroundColor = UIColor.orange
        self.videoViewController = UINavigationController(rootViewController: videoViewController)
    }
    
    /*
     * This way we deallocate ViewController after changing to another ViewController, so we don't keep it in memory all the time
     */
    func deallocateDynamicallyCreatedViewControllers() {        
        self.videoViewController = nil
    }
    
    func changeViewController(menu: LeftMenu) {
        deallocateDynamicallyCreatedViewControllers()
        
        switch menu {
        case .View:
            self.revealViewController().pushFrontViewController(self.mainViewController, animated: true)
            break
        case .Photo:
            self.revealViewController().pushFrontViewController(self.ePrescriptionViewController, animated: true)
            break
        case .Map:
            self.revealViewController().pushFrontViewController(self.mapViewController, animated: true)
            break
        case .Blah:
            self.revealViewController().pushFrontViewController(self.blahViewController, animated: true)
            break
        case .Video:
            instantiateDynamicallyCreatedViewControllers()
            self.revealViewController().pushFrontViewController(self.videoViewController, animated: true)
            break
        }
    }
    
}

