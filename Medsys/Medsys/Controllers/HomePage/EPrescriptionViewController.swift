//
//  EPrescriptionViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/5/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class EPrescriptionViewController: UIViewController {

    var patientAppointmentDetails : AppointmentDetails!
    var patientId : String!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    
    @IBOutlet weak var segmentView: UISegmentedControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    var medicalHistory : [String] = []
    var prescription : [String] = []
    var cellCount : Int = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let appColor = UIColor.init(hex: "0E2C59")

        navigationItem.title = "E-Prescription"
        navigationController?.navigationBar.barTintColor = appColor
        navigationController?.navigationBar.barStyle = .black
        
        medicalHistory = ["Alergies", "Medication","Problem"]
        prescription = ["Prescription","Notification"]
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let id = patientAppointmentDetails?.patientId!
        patientId = id!
        
//        setNavigationBarItemLeft()
        getEPrescriptionData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getEPrescriptionData() -> Void {
        
        let requestType = "MainInfo"
        
        AccountManager.Global.getEPrescriptionData(patientId: patientId, requestType: requestType) { (success, prescriptionData, allergyData, medicationData, ProblemData, notificationData, responceData) in
            
            print("PrescriptionData :- \(prescriptionData)")
            print("AllergyData :- \(allergyData)")
            print("MedicationData :- \(medicationData)")
            print("ProblemData :- \(ProblemData)")
            print("NotificationData :- \(notificationData)")
        }
    }
    
    @IBAction func segmentTapped(_ sender: Any) {
        
        switch segmentView.selectedSegmentIndex {
        case 0:
            cellCount = 3
            tableView.reloadData()
        case 1:
            cellCount = 2
            tableView.reloadData()
            
        default:
            break
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EPrescriptionViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if cellCount == 2
        {
            return 2
        }
        else if cellCount == 3
        {
            return 3
        }
        else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "epriscriptionCell") as! EpriscriptionTableViewCell
        cell.selectionStyle = .none
        
        if cellCount == 3
        {
            cell.prescrip = medicalHistory [indexPath.row]
        }
        else if cellCount == 2
        {
            cell.notification = prescription [indexPath.row]

        }
        
        return cell
    }
}

extension EPrescriptionViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


