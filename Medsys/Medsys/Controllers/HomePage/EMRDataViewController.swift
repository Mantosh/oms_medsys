//
//  EMRDataViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/21/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class EMRDataViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var patientNameLabel: UILabel!
    @IBOutlet weak var patientIdLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var emrDataList : [EMRData] = []
    
    var patientId : String!
    var patientFirstName : String!
    var patientLastName : String!
    var backItem = UIBarButtonItem()
    var patientAppointmentDetails : AppointmentDetails!

    @IBOutlet weak var noDataFound: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let appColor = UIColor.init(hex: "0E2C59")

        topView.addShadow(radius: 3.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)

        navigationItem.title = "EMR Data"
        navigationController?.navigationBar.barTintColor = appColor
        navigationController?.navigationBar.barStyle = .black
        
        //---
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.white
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self?.getEMRData()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.tableView.dg_stopLoading()
            })
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(appColor)
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
        //---
        
        let fName = patientAppointmentDetails?.patientFirstName!
        let lName = patientAppointmentDetails?.patientLastName!
        let id = patientAppointmentDetails?.patientId!
        
        patientFirstName = fName!
        patientLastName = lName!
        patientId = id!
        
        patientNameLabel.text = "\(String(describing: patientFirstName!)) \(String(describing: patientLastName!))"
        patientIdLabel.text = "Patient id :\(patientId!)"

        noDataFound.isHidden = true
        getEMRData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        noDataFound.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        clearSwRevealViewControllerMenuOptions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getEMRData()
    {
        
//        startAnimating(CGSize(width: 50, height:50), message: "", type: NVActivityIndicatorType.lineScalePulseOut)

        let requestType = "EMR"

        activityIndicator.isHidden = false
        activityIndicator.startAnimating()

        AccountManager.Global.getPatientEMRData(patientId: patientId, requestType: requestType) { (success, jsonData, responceData) in
            
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()

//            self.stopAnimating()
            print("jsonData- EMR --- \(jsonData)")
            
            self.emrDataList = jsonData as! [EMRData]
            
            if self.emrDataList.count == 0
            {
                self.noDataFound.isHidden = false
            }else {
                self.noDataFound.isHidden = true
            }
            
            self.tableView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension EMRDataViewController : EMRCellDelegate
{
    func didSelectEMRInfo(_cell: EMRTableViewCell, emrData: String) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "EMRWebViewViewController") as! EMRWebViewViewController
        
        viewCtr.emrHtmldata = emrData
        
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
        navigationController?.pushViewController(viewCtr, animated: true)
    }
}

extension EMRDataViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return emrDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "emrCell") as! EMRTableViewCell
        cell.selectionStyle = .none
        
        cell.emrData = emrDataList [indexPath.row]
        cell.delegate = self
        
        return cell
    }
}

extension EMRDataViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
}


