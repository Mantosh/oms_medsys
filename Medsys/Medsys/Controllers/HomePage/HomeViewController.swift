///
//  HomeViewController.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/14/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit
import DropDown

enum AppointmentDateType : String {
    case today
    case thisWeek
    case NextWeek
    case SelectDate
}

class HomeViewController: UIViewController, SMDatePickerDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var segmentView: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var officeButton: UIButton!
    
    @IBOutlet weak var tableTopCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var requestType : String!
    var officeId : String!
    var isToDateSelected : Bool!
    var isFromDateSelected : Bool!
    var strFromDate : String!
    var strToDate : String!
    
    var dateSigmentSelected : Bool = false
    
    var appointmentList : [AppointmentDetails] = []
    
//    var office
    
    @IBOutlet weak var fromDateText: UITextField!
    @IBOutlet weak var toDateText: UITextField!
    @IBOutlet weak var datePickView: UIView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    
    let datePicker = UIDatePicker()
    
    let date = Date()
    let formatter = DateFormatter()
    
    var backItem = UIBarButtonItem()

    @IBOutlet weak var noDataFoundLabel: UILabel!
    
    var officeList : [JsonDecodable] = []
    
    // drop down
    var officeDropDown = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.officeDropDown,
            ]
    }()
    
    var selectedIndex : NSInteger! = -1 //Delecre this global

    
    var expandedRows = Set<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let appColor = UIColor.init(hex: "0E2C59")
        
//        topView.addShadow(radius: 3.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 1.0), maskToBounds: false)
        setNavigationBarItem()
    
        navigationItem.title = "Appointments"
        navigationController?.navigationBar.barTintColor = UIColor.init(hex: "10386B")
        navigationController?.navigationBar.barStyle = .black
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.officeButton.designLayer(cornerRadius: 3.0, borderColor: UIColor.white, borderWidth: 1.0, maskToBounds: true)
        searchButton.designLayer(cornerRadius: 3.0, borderColor: UIColor.white, borderWidth: 1.0, maskToBounds: true)
        
        noDataFoundLabel.isHidden = true
//        tableView.isHidden = true
        datePickView.isHidden = true
        
        // call today appointment data
        formatter.dateFormat = "MM/dd/yyyy"
        
        requestType = "DateRange"
        officeId = "155"
        isToDateSelected = false
        isFromDateSelected = false
        
        strFromDate = formatter.string(from: date)
        strToDate = formatter.string(from: date)
        
        UserDefaults.standard.set("Today", forKey: "requestType")
        showDatePicker()
        
        //---
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = appColor
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            
            self?.getAppointmentData()
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                self?.tableView.dg_stopLoading()
            })
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(UIColor.white)
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
        //---
        
        
//        // tap gesture on screen
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
//            target: self,
//            action: #selector(HomeViewController.dismissKeyboard))
        
//        view.addGestureRecognizer(tap)
        
        print(officeList)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        doctorNameLabel.text = UserDefaults.standard.value(forKey: "userId") as! String
        tableTopCOnstraint.constant = 10
        
        customizeDropDown(self)

        selectedIndex = -1
        tableView.beginUpdates()
        tableView.endUpdates()
        tableView.setContentOffset(.zero, animated: true)
        segmentView.selectedSegmentIndex = 0

        setupOfficeDropDown()
        
        // call api
        getAppointmentData()
    }
    
    func showDatePicker(){
        
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.backgroundColor =  UIColor.init(hex: "EFF3F8")
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.barTintColor = UIColor( red: 0/255, green: 136/255, blue:188/255, alpha: 1.0 )
        toolbar.tintColor = UIColor.white
        toolbar.sizeToFit()
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let doneButton = UIBarButtonItem(
            title: "Done",
            style: .plain,
            target: self,
            action: #selector(donedatePicker(sender:))
        )
        
        let cancelButton = UIBarButtonItem(
            title: "Cancel",
            style: .plain,
            target: self,
            action: #selector(cancelDatePicker(sender:))
        )
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

        fromDateText.inputAccessoryView = toolbar
        fromDateText.inputView = datePicker
        
        toDateText.inputAccessoryView = toolbar
        toDateText.inputView = datePicker
    }
    
    @objc func donedatePicker(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    @IBAction func searchTapped(_ sender: Any) {
        
        if isToDateSelected && isFromDateSelected
        {
            strFromDate = fromDateText.text
            strToDate = toDateText.text
            
            getAppointmentData()
        }
        else
        {
            let alert = UIAlertController.init(title: "Message", message: "Please select To date and from date", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func segmentTapped(_ sender: Any) {
        
        switch segmentView.selectedSegmentIndex
        {
        case 0:
            tableTopCOnstraint.constant = 10
            requestType = "DateRange"
            
            strFromDate = formatter.string(from: date)
            strToDate = formatter.string(from: date)
            
            noDataFoundLabel.isHidden = true
//            tableView.isHidden = true

            dateSigmentSelected = false
            datePickView.isHidden = true

            getAppointmentData()
            
        case 1:
            tableTopCOnstraint.constant = 10
            requestType = "ThisWeek"
            
            strFromDate = ""
            strToDate = ""

            noDataFoundLabel.isHidden = true
//            tableView.isHidden = true

            dateSigmentSelected = false
            datePickView.isHidden = true

            getAppointmentData()
            
        case 2:
            tableTopCOnstraint.constant = 10
            requestType = "NextWeek"
            
            strFromDate = ""
            strToDate = ""

            noDataFoundLabel.isHidden = true
//            tableView.isHidden = true
            datePickView.isHidden = true
            dateSigmentSelected = false

            getAppointmentData()
            
        case 3:
            datePickView.isHidden = false
            
            tableTopCOnstraint.constant = 65
            requestType = "DateRange"
            
            noDataFoundLabel.isHidden = true
//            tableView.isHidden = true
            
            dateSigmentSelected = true
            
        default:
            datePickView.isHidden = true
            tableTopCOnstraint.constant = 10
            break
        }
    }
    
    @IBAction func officeChooseClicked(_ sender: Any) {
        
        officeDropDown.show()
    }
    
    func getAppointmentData() -> Void {
        
        activityIndicator.isHidden = false
        noDataFoundLabel.isHidden = true

        activityIndicator.startAnimating()


        AccountManager.Global.getAppointmentData( officeId: officeId, strToDate : strToDate, strFromDate: strFromDate, requestType : requestType) { (success, jsonData, responceData) in
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true

            self.appointmentList = jsonData as! [AppointmentDetails]
            
            self.selectedIndex = -1
            
            if self.appointmentList.count == 0
            {
                self.noDataFoundLabel.isHidden = false
            }
            else {
                self.noDataFoundLabel.isHidden = true
            }

            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        clearSwRevealViewControllerMenuOptions()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func dismissKeyboard()
    {
        //self.view.endEditing(true)
    }
    
    func setupOfficeDropDown()
    {
        officeDropDown.anchorView = officeButton
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        officeDropDown.bottomOffset = CGPoint(x: 0, y: officeButton.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        officeDropDown.dataSource = [
            "Raynham",
            "Taunton",
            "Lakeville",
        ]
        
        // Action triggered on selection
        officeDropDown.selectionAction = { [weak self] (index, item) in
            
            if item == "Raynham"
            {
                self?.officeId = "155"
            }
            else if item == "Taunton"
            {
                self?.officeId = "186"
            }
            else if item == "Lakeville"
            {
                self?.officeId = "205"
            }
           
            self?.officeButton.setTitle(item, for: .normal)
            self?.getAppointmentData()

        }
    }
}

func customizeDropDown(_ sender: AnyObject) {
    let appearance = DropDown.appearance()
    
    appearance.cellHeight = 44
    appearance.backgroundColor = UIColor(white: 1, alpha: 1)
    appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
    appearance.separatorColor = UIColor(white: 0.7, alpha: 0.8)
    appearance.cornerRadius = 5
    appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
    appearance.shadowOpacity = 0.9
    appearance.shadowRadius = 10
    appearance.animationduration = 0.25
    appearance.textColor = .darkGray
    //        appearance.textFont = UIFont(name: "Georgia", size: 14)
    
    //        dropDowns.forEach {
    //            /*** FOR CUSTOM CELLS ***/
    //            $0.cellNib = UINib(nibName: "MyCell", bundle: nil)
    //
    //            $0.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
    //                guard let cell = cell as? MyCell else { return }
    //
    //                // Setup your custom UI components
    ////                cell.suffixLabel.text = "Suffix \(index)"
    //            }
    //            /*** ---------------- ***/
    //        }
}

//================ drop Down end

extension HomeViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if appointmentList.count == 0
        {
//            noDataFoundLabel.isHidden = false
//            tableView.isHidden = true

            if dateSigmentSelected
            {
                datePickView.isHidden = false
            }else {
                datePickView.isHidden = true
            }
        }
        else
        {
//            noDataFoundLabel.isHidden = true
//            tableView.isHidden = false
        }
        return appointmentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "apointCell") as! HomeTableViewCell
        
        cell.appointment = appointmentList [indexPath.row]
        cell.delegate = self
        
        cell.selectionStyle = .none
        return cell
    }
}

extension HomeViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? HomeTableViewCell
            else { return }
        
        if indexPath.row == selectedIndex{
            selectedIndex = -1
        }else{
            selectedIndex = indexPath.row
        }
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell = tableView.cellForRow(at: indexPath) as? HomeTableViewCell
        
        if indexPath.row == selectedIndex
        {
            cell?.nextArrowImg.image = #imageLiteral(resourceName: "arrowDown")
            return 160
        }else{
            cell?.nextArrowImg.image = #imageLiteral(resourceName: "arrowNext")
            return 115
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? HomeTableViewCell
            else { return }
        
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
}

extension HomeViewController : homeCellDelegate
{
    func btnEMRTapped(cell: HomeTableViewCell, emrDetails: AppointmentDetails) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "EMRDataViewController") as! EMRDataViewController
        
        viewCtr.patientAppointmentDetails = emrDetails

        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationController?.pushViewController(viewCtr, animated: true)
    }
    
    func btnGeoInfoTapped(cell: HomeTableViewCell, geoDetails: AppointmentDetails) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "PatientGeoInfoViewController") as! PatientGeoInfoViewController
        viewCtr.patientAppointmentDetails = geoDetails

        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        
        navigationController?.pushViewController(viewCtr, animated: true)
    }
    
    func btnHistoryInfoTapped(cell: HomeTableViewCell, historyDetails: AppointmentDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "PatientHistoryInfoViewController") as! PatientHistoryInfoViewController
        viewCtr.patientAppointmentDetails = historyDetails

        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationController?.pushViewController(viewCtr, animated: true)
    }
    
    func btnAppointmentInfoTapped(cell: HomeTableViewCell, fullDetails: AppointmentDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "PatientAppointmentDetailsViewController") as! PatientAppointmentDetailsViewController
        
        viewCtr.patientAppointmentDetails = fullDetails

        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationController?.pushViewController(viewCtr, animated: true)
    }
    
    func btnEPriscriptionTapped(cell: HomeTableViewCell, fullDetails: AppointmentDetails) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewCtr = storyboard.instantiateViewController(withIdentifier: "EPrescriptionViewController") as! EPrescriptionViewController
        
        viewCtr.patientAppointmentDetails = fullDetails
        
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        navigationItem.backBarButtonItem?.tintColor = UIColor.white
        navigationController?.pushViewController(viewCtr, animated: true)
    }
}

extension HomeViewController : UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        if textField == fromDateText
        {
            isFromDateSelected = true
            fromDateText.text = formatter.string(from: datePicker.date)
            strFromDate = fromDateText.text
        }
        else if textField == toDateText
        {
            isToDateSelected = true
            toDateText.text = formatter.string(from: datePicker.date)
            strToDate = toDateText.text
        }
    }
}
