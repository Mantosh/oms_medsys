//
//  BuildEnvironment.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 2/13/18.
//  Copyright © 2018 ThirdEye. All rights reserved.
//

import Foundation

/// This defines the current build environment.
enum BuildEnvironment {
    case development, production, adhoc
    
    #if DEVELOPMENT
    static let current = BuildEnvironment.development
    #elseif APPSTORE
    static let current = BuildEnvironment.production
    #else
    static let current = BuildEnvironment.adhoc
    #endif
}
