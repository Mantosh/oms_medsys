//
//  View.swift
//  Medsys 
//
//  Created by Mantosh Kumar on 2/14/18.
//  Copyright © 2018 mantosh. All rights reserved.
//
//

import UIKit

extension UIView
{
    func designLayer (cornerRadius : CGFloat = 0, borderColor : UIColor? = nil, borderWidth : CGFloat = 0, maskToBounds : Bool = true) -> Void {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = maskToBounds
        if borderWidth > 0.0 {
            layer.borderWidth = borderWidth
            if let borderColor = borderColor {
                layer.borderColor = borderColor.cgColor
            }
        }
    }
    
    func addShadow(radius : CGFloat , opacity : Float, color : UIColor, offset : CGSize = CGSize.zero, maskToBounds : Bool = false)
    {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.masksToBounds = maskToBounds
    }

//    func showSnackbar (with message : String?, duration : SnackbarDuration = .middle, animationType : SnackbarAnimationType = .slideFromBottomBackToBottom) -> Void {
//
//        if let barMessage = message?.trimmingCharacters(in: .whitespacesAndNewlines), !barMessage.isEmpty {
//            let snackbar = Snackbar.init(message: barMessage, duration: duration)
//            snackbar.containerView = self
//            snackbar.animationType = animationType
//            snackbar.messageTextFont = UIFont.init(name: "OpenSans", size: 15)! //InfoCell.detailsFont
//            snackbar.bottomMargin = 44
//            snackbar.topMargin = 44
//            snackbar.show()
//        }
//    }
}
