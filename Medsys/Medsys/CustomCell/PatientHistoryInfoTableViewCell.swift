//
//  PatientHistoryInfoTableViewCell.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class PatientHistoryInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var appointmentCodeLabel: UILabel!
    @IBOutlet weak var appointmentTypeLabel: UILabel!
    @IBOutlet weak var prescriptionLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    
    var history : PatientHistoryInfo?
    {
        didSet{
            
            dateLabel.text = history?.appointmentDate
            appointmentCodeLabel.text = history?.appointmentCode
            appointmentTypeLabel.text = history?.appointmentName
            prescriptionLabel.text = history?.columnDescription
            noteLabel.text = history?.note
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
