//
//  PatientGeoInfoTableViewCell.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class PatientGeoInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ssnLabel: UILabel!
    @IBOutlet weak var homePhoneLabel: UILabel!
    @IBOutlet weak var mobilePhoneLabel: UILabel!
    @IBOutlet weak var patientAddressLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    
    @IBOutlet weak var referralNameLabel: UILabel!
    @IBOutlet weak var guarantorNameLabel: UILabel!
    
    @IBOutlet weak var emergencyNameLabel: UILabel!
    @IBOutlet weak var emergencyPhoneLabel: UILabel!
    
    var refFName1 : String!
    var refLName1 : String!
    
    var geoData : PatientGeoInfo?
    {
        didSet
        {
            if let dob = geoData?.dateOfBirth
            {
                let intAge = self.calcAge(birthday: dob)
                ageLabel.text = String (intAge)
            }
            
            genderLabel.text = geoData?.sex
            ssnLabel.text = geoData?.SSN
            homePhoneLabel.text = geoData?.homePhone
            mobilePhoneLabel.text = geoData?.mobilePhone
            patientAddressLabel.text = geoData?.patientAddress
            stateLabel.text = geoData?.state
            cityLabel.text = geoData?.city
            zipLabel.text = geoData?.zipCode
            
            let refFName = geoData?.referralFirstName!
            let refLName = geoData?.referralLastName!
            
            if (refFName != nil) && (refLName != nil)
            {
                let referralName = "\(String(describing: refFName!)) \(String(describing: refLName!))"
                referralNameLabel.text = referralName
            }
            
            let guarFName = geoData?.gauranatorFirstName!
            let guarLName = geoData?.gauranatorLastName!
            
            if (guarFName != nil) && (guarLName != nil)
            {
                let referralName = "\(String(describing: guarFName!)) \(String(describing: guarLName!))"
                guarantorNameLabel.text = referralName
            }
            
            
            emergencyNameLabel.text = geoData?.emergencyContactName
            emergencyPhoneLabel.text = geoData?.emergencyContactPhone
            
        }
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM/dd/yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
