//
//  EMRTableViewCell.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/22/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

protocol EMRCellDelegate : class {
    
    func didSelectEMRInfo(_cell : EMRTableViewCell , emrData : String)
}

class EMRTableViewCell: UITableViewCell {
    
    @IBOutlet weak var reasonToVisitLabel: UILabel!
    @IBOutlet weak var addedByLabel: UILabel!
    @IBOutlet weak var encounterDateLabel: UILabel!
    
    @IBOutlet weak var emrDetalisButton: UIButton!
    
    weak var delegate : EMRCellDelegate?
    
    var emrData : EMRData?
    {
        didSet
        {            
            reasonToVisitLabel.text = emrData?.reasonToVisit
            addedByLabel.text = emrData?.addedBy
            encounterDateLabel.text = emrData?.encounterDate
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func emrDetailsTapped(_ sender: UIButton) {
        
        delegate?.didSelectEMRInfo(_cell: self, emrData: (emrData?.encounterSummary)!)
    }
    
}
