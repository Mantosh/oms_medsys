//
//  EpriscriptionTableViewCell.swift
//  Medsys
//
//  Created by Mantosh Kumar on 3/7/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

class EpriscriptionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var ePriscriptionType: UILabel!
    @IBOutlet weak var cellBGView: UIView!
    
   var  medicalHistory = ["Alergies", "Medication","Problem"]
    var prescription = ["Prescription","Notification"]

    
    var prescrip : String!
    {
        didSet
        {
            ePriscriptionType.text = prescrip
        }
    }
    
    var notification : String!
    {
        didSet
        {
            ePriscriptionType.text = notification
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
