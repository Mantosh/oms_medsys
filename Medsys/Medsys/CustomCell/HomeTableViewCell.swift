//
//  HomeTableViewCell.swift
//  Medsys
//
//  Created by Mantosh Kumar on 2/14/18.
//  Copyright © 2018 mantosh. All rights reserved.
//

import UIKit

protocol homeCellDelegate : class {
    func btnEMRTapped( cell : HomeTableViewCell, emrDetails : AppointmentDetails)
    func btnGeoInfoTapped( cell : HomeTableViewCell, geoDetails : AppointmentDetails)
    func btnHistoryInfoTapped( cell : HomeTableViewCell, historyDetails : AppointmentDetails)
    func btnAppointmentInfoTapped( cell : HomeTableViewCell, fullDetails : AppointmentDetails)
    func btnEPriscriptionTapped( cell : HomeTableViewCell, fullDetails : AppointmentDetails)

}

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var cellBgView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var fromTimeLabel: UILabel!
    @IBOutlet weak var toTimeLabel: UILabel!
    @IBOutlet weak var durationTimeLabel: UILabel!

    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var problemLabel: UILabel!
    
    @IBOutlet weak var appointmentType: UILabel!
    @IBOutlet weak var constraintCellHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tooBgView: UIView!
    @IBOutlet weak var noteLabel: UILabel!
    
    @IBOutlet weak var emrButton: UIButton!
    @IBOutlet weak var patientGeoInfoButton: UIButton!
    @IBOutlet weak var paitentPreviousHistoryButton: UIButton!
    @IBOutlet weak var paitentAppointmentDetailsButton: UIButton!
    @IBOutlet weak var ePrescriptionButton: UIButton!

    
    @IBOutlet weak var nextArrowImg: UIImageView!
    var delegate : homeCellDelegate?
    
    var startDate : Date!
    var appEndDate : Date!
    
    var stTime : String!
    var endTime : String!
    
    var dateFormatter = DateFormatter()

    var officeList : [JsonDecodable] = []
    
    var appointment : AppointmentDetails?
    {
        didSet {
            
            if let appoint = appointment
            {
                
                doctorNameLabel.text = "Patient    : \(appoint.patientFirstName!) \(appoint.patientLastName!)"
                
                let prob = "\(String(describing: appoint.appointmentName!))/ \(String(describing: appoint.columnDescription!))"
                problemLabel.text = prob
                
                if appoint.note == ""
                {
                    noteLabel.text = "   -"
                }
                else
                {
                    noteLabel.text = appoint.note
                }
                
//                let sTime = changeStringToMinut(sTime: appoint.startTime!)
//                
//                print(sTime)
                
                if let sTime = appoint.startTime
                {
                    let time = sTime
                    if time == "0"
                    {
                        stTime = "00:00"
                    }
                    else if time.count == 2
                    {
                        stTime = "00\(time)"
                        stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
                    }
                    else if time.count == 3
                    {
                        stTime = "0\(time)"
                        stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
                    }
                    else
                    {
                        stTime = time

                        stTime.insert(":", at: stTime.index(stTime.endIndex, offsetBy: -2))
                    }
                }
                
                if let eTime = appoint.endTime
                {
                    let time = eTime
                    if time == "0"
                    {
                        endTime = "00:00"
                    }
                    else if time.count == 2
                    {
                        endTime = "00\(time)"
                        endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
                    }
                    else if time.count == 3
                    {
                        endTime = "0\(time)"
                        endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
                    }
                    else
                    {
                        endTime = time
                        endTime.insert(":", at: endTime.index(endTime.endIndex, offsetBy: -2))
                    }
                    
                }
                
                print(" Appointment time -> \(stTime!) - \(endTime!)")
                
                let  sTime1 = self.changeTimeformet(time: stTime)
                let  eTime1 = self.changeTimeformet(time: endTime)

                let duration = changeTimeToDateFormete(fromTime: stTime, toTime: endTime)
                
                print(" Appointment duration -> \(duration as! String)")
                
                durationTimeLabel.text = "\(duration)Min"
                
                fromTimeLabel.text = sTime1
                toTimeLabel.text = eTime1
                
                if let dateString = appoint.appointmentDate
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yyyy"
                    
                    let date2 = dateFormatter.date(from: dateString)
                    let modifiedTime = TimeUtil.Global.monthTimeString(from: date2)
                    
                    monthLabel.text = modifiedTime
                }
                
                if appoint.confirmed == "True"
                {
                    monthLabel.textColor = UIColor.init(hex: "5D9324")
                    fromTimeLabel.textColor = UIColor.init(hex: "5D9324")
                    toTimeLabel.textColor = UIColor.init(hex: "5D9324")
                    durationTimeLabel.textColor = UIColor.init(hex: "5D9324")
                }
            }
        }
    }
    
    func changeStringToMinut(sTime : String) -> String {
        
        var mofifiedTime : String!
        
        if sTime == "0"
        {
            mofifiedTime = "00:00"
        }
        else if sTime.count == 2
        {
            mofifiedTime = "00\(time)"
            mofifiedTime.insert(":", at: sTime.index(sTime.endIndex, offsetBy: -2))
        }
        else if sTime.count == 3
        {
            mofifiedTime = "0\(time)"
            mofifiedTime.insert(":", at: sTime.index(sTime.endIndex, offsetBy: -2))
        }
        else
        {
            mofifiedTime.insert(":", at: sTime.index(sTime.endIndex, offsetBy: -2))
        }
        
        return mofifiedTime
    }
    
    func changeTimeToDateFormete(fromTime : String, toTime : String) -> String {
        
        dateFormatter.dateFormat = "H:mm"
        let fTime = dateFormatter.date(from: fromTime)!
        let tTime = dateFormatter.date(from: toTime)!
        
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([ .minute])
        let datecomponenets = calendar.dateComponents(unitFlags, from: fTime, to: tTime)
        let minutes = String(describing: datecomponenets.minute!)
        
        return minutes
    }

    func changeTimeformet(time : String) -> String {
        
        dateFormatter.dateFormat = "H:mm"
        let date12 = dateFormatter.date(from: time)!
        
        dateFormatter.dateFormat = "h:mm a"
        let date22 = dateFormatter.string(from: date12)
        
        return String(date22)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let appColor = UIColor.init(hex: "0E2C59")

//        cellView.addShadow(radius: 1.0, opacity: 1.0, color: UIColor.lightGray, offset: CGSize(width : 0.0, height : 2.0), maskToBounds: true)
        
        cellView.designLayer(cornerRadius: 5.0, borderColor: UIColor.white, borderWidth: 0.8, maskToBounds: true)
        
    }
    
    @IBAction func emrButtonClicked(_ sender: Any) {
        
        if let _ = delegate {
            
            delegate?.btnEMRTapped(cell: self, emrDetails: appointment!)
        }
    }
    
    @IBAction func patientGeoInfoButtonClicked(_ sender: Any) {
        if let _ = delegate {
            
            delegate?.btnGeoInfoTapped(cell: self, geoDetails: appointment!)
        }
    }
    
    @IBAction func paitentPreviousHistoryButtonClicked(_ sender: Any) {
        if let _ = delegate {
            
            delegate?.btnHistoryInfoTapped(cell: self, historyDetails: appointment!)

        }
    }
    
    @IBAction func paitentAppointmentDetails(_ sender: Any) {
        if let _ = delegate {
            
            delegate?.btnAppointmentInfoTapped(cell: self, fullDetails: appointment!)
        }
    }
    
    @IBAction func ePrescriptionTapped(_ sender: Any) {
        
        if let _ = delegate
        {
            delegate?.btnEPriscriptionTapped(cell: self, fullDetails: appointment!)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}



