 //
//  AccountManager.swift
//  AtoZdatabase
//
//  Created by Mantosh Kumar on 2/13/18.
//  Copyright © 2018 ThirdEye. All rights reserved.
//

import UIKit
import CoreTelephony
import CoreLocation
import JASON

class AccountManager: NSObject {
    
    static let Global = AccountManager()
    
    var account : Account? = nil
    
    let requestManager : RequestManager
    
    override init() {

        requestManager = RequestManager.Global
        super.init()
    }
    

    func login(userName : String, password : String, deviceType : String, completion : @escaping Completion.GetRegistor) -> Void {
        
        let header : [String : String] = [HTTPHeaders.ContentType : MimeType.Json]
        let postData : [String : String] = ["userName" : userName,
                                            "password" : password,
                                            "DeviceType" : deviceType]
        
        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/login"
        let urlString = URL(string: stringURL)
        
        let _ = requestManager.JsonPostRequest(withUrl: urlString!, urlParameters: nil, headers: header, postData: postData) { (jsonData, error, responceCode) in
            
            print("JsonData :- \(String(describing: jsonData))")
            
            DispatchQueue.main.async(execute: {
                
                if error != nil
                {
                    completion(false, nil, "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.InvalidUserCode
                {
                    completion (false, nil,  "Please enter valid username / password")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        
                    }
                    
                    completion(true, jsonData, "")
                }
                else
                {
                    completion(false, nil, "Invalid response received from server")
                }
            })
        }
    
    }
    
    func getAppointmentData(officeId : String, strToDate : String, strFromDate : String,  requestType : String, completion : @escaping Completion.Appointment ) -> Void
    {
        
        let token = UserDefaults.standard.value(forKey: "tokenValue")
        let deviceId = UserDefaults.standard.value(forKey: "deviceId")
        let userId = UserDefaults.standard.value(forKey: "userId")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        let providerId = UserDefaults.standard.value(forKey: "providerId")
        
        let deviceType = "mobile"

        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/DashBoard_Data"
        let urlString = URL(string: stringURL)
        
        let headers : [String : String] = [HTTPHeaders.ContentType                      : MimeType.Json,
                                           HTTPHeaders.AcceptMimeType                   : MimeType.Json,
                                           "apikey"                                     : token as! String,
                                           "deviceId"                                   : deviceId as! String,
                                           "userId"                                     : userId as! String,
                                           "deviceType"                                 : deviceType]
        
        let urlParams : [String : String] = ["ProviderId"             :   providerId as! String,
                                             "PracticeId"             :   practiceId as! String,
                                             "RequestType"            :   requestType,
                                             "OfficeId"               :   officeId,
                                             "strFromDate"            :   strFromDate,
                                             "strToDate"              :   strToDate]
        
        let _ = requestManager.JsonGetRequest(withURL: urlString!, urlParameters: urlParams, headers: headers) { [weak self] (jsonData, error, responceCode) in
            
            DispatchQueue.main.async(execute: {
                
                print("jsonData :- \(String(describing: jsonData))")

                if error != nil
                {
                    completion(false, [], "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        let appointJsonList = json ["Appointments"].jsonArray
                        let appointment = AppointmentDetails.ArrayFromJsonFactroy(appointJsonList)
                        completion(true, appointment, "")
                    }
                }
                else
                {
                    completion(true, [], "Invalid response received from server")
                }
            })
        }
    }
    
    func getPatientEMRData(patientId : String , requestType : String, completion : @escaping Completion.Appointment ) -> Void
    {
        
        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/DashBoardDetails"
        let urlString = URL(string: stringURL)
        
        let token = UserDefaults.standard.value(forKey: "tokenValue")
        let deviceId = UserDefaults.standard.value(forKey: "deviceId")
        let userId = UserDefaults.standard.value(forKey: "userId")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        
        let deviceType = "mobile"

        
        
        let headers : [String : String] = [HTTPHeaders.ContentType                      : MimeType.Json,
                                           HTTPHeaders.AcceptMimeType                   : MimeType.Json,
                                           "apikey"                                     : token as! String,
                                           "deviceId"                                   : deviceId as! String,
                                           "userId"                                     : userId as! String,
                                           "deviceType"                                 : deviceType]
        
        let urlParams : [String : String] = ["PracticeId"             :   practiceId as! String,
                                             "RequestType"            :   requestType,
                                             "PatientId"              :   patientId,
                                             ]
        
        
        let _ = requestManager.JsonGetRequest(withURL: urlString!, urlParameters: urlParams, headers: headers) { [weak self] (jsonData, error, responceCode) in
            
            DispatchQueue.main.async(execute: {
                
                print("jsonData :- \(String(describing: jsonData))")
                
                if error != nil
                {
                    completion(false, [], "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        print("json - - \(json)")
                        let resultJsonList = json ["Result"].jsonArray
                        
                        print("resultJsonList--\(resultJsonList)")
                        
                        let appointment = EMRData.ArrayFromJsonFactroy(resultJsonList)
                        completion(true, appointment, "")
                    }
                }
                else
                {
                    completion(true, [], "Invalid response received from server")
                }
            })
        }
    }
    
    func getPatientHistoryData(practiceId : String, patientId : String , requestType : String, completion : @escaping Completion.Appointment ) -> Void
    {
        
        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/DashBoardDetails"
        let urlString = URL(string: stringURL)
        
        let token = UserDefaults.standard.value(forKey: "tokenValue")
        let deviceId = UserDefaults.standard.value(forKey: "deviceId")
        let userId = UserDefaults.standard.value(forKey: "userId")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        
        let deviceType = "mobile"


        
        let headers : [String : String] = [HTTPHeaders.ContentType                      : MimeType.Json,
                                           HTTPHeaders.AcceptMimeType                   : MimeType.Json,
                                           "apikey"                                     : token as! String,
                                           "deviceId"                                   : deviceId as! String,
                                           "userId"                                     : userId as! String,
                                           "deviceType"                                 : deviceType]
        
        let urlParams : [String : String] = ["PracticeId"             :   practiceId as! String,
                                             "RequestType"            :   requestType,
                                             "PatientId"              :   patientId,
                                             ]
        
        
        let _ = requestManager.JsonGetRequest(withURL: urlString!, urlParameters: urlParams, headers: headers) { [weak self] (jsonData, error, responceCode) in
            
            DispatchQueue.main.async(execute: {
                
                print("jsonData :- \(String(describing: jsonData))")
                
                if error != nil
                {
                    completion(false, [], "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        let resultJsonList = json ["Result"].jsonArray
                        
                        let appointment = PatientHistoryInfo.ArrayFromJsonFactroy(resultJsonList)
                        completion(true, appointment, "")
                    }
                }
                else
                {
                    completion(true, [], "Invalid response received from server")
                }
            })
        }
    }
    
    
    func getPatientGeoData(patientId : String , requestType : String, completion : @escaping Completion.AppointmentGeoInfo ) -> Void
    {
        
        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/DashBoardDetails"
        let urlString = URL(string: stringURL)
        
        let token = UserDefaults.standard.value(forKey: "tokenValue")
        let deviceId = UserDefaults.standard.value(forKey: "deviceId")
        let userId = UserDefaults.standard.value(forKey: "userId")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        
        let deviceType = "mobile"


        
        let headers : [String : String] = [HTTPHeaders.ContentType                      : MimeType.Json,
                                           HTTPHeaders.AcceptMimeType                   : MimeType.Json,
                                           "apikey"                                     : token as! String,
                                           "deviceId"                                   : deviceId as! String,
                                           "userId"                                     : userId as! String,
                                           "deviceType"                                 : deviceType]
        
        let urlParams : [String : String] = ["PracticeId"             :   practiceId as! String,
                                             "RequestType"            :   requestType,
                                             "PatientId"              :   patientId,
                                             ]
        
        
        let _ = requestManager.JsonGetRequest(withURL: urlString!, urlParameters: urlParams, headers: headers) { [weak self] (jsonData, error, responceCode) in
            
            DispatchQueue.main.async(execute: {
                
                if error != nil
                {
                    completion(false, nil , "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        print("json - - \(json)")
                    
                        let appointment = PatientGeoInfo (json : json )
                        
                        print(appointment)
                        
                        completion(true, appointment, "")
                    }
                }
                else
                {
                    completion(true, nil , "Invalid response received from server")
                }
            })
        }
    }
    
    func getEPrescriptionData(patientId : String , requestType : String, completion : @escaping Completion.Eprescrtiption ) -> Void
    {
        
        let stringURL : String!
        stringURL = "https://pm4-restapi-ppd.onlinemedsys.com/api/Prescription"
        let urlString = URL(string: stringURL)
        
        let token = UserDefaults.standard.value(forKey: "tokenValue")
        let deviceId = UserDefaults.standard.value(forKey: "deviceId")
        let userId = UserDefaults.standard.value(forKey: "userId")
        let practiceId = UserDefaults.standard.value(forKey: "practiceId")
        
        let deviceType = "mobile"


        
        let headers : [String : String] = [HTTPHeaders.ContentType                      : MimeType.Json,
                                           HTTPHeaders.AcceptMimeType                   : MimeType.Json,
                                           "apikey"                                     : token as! String,
                                           "deviceId"                                   : deviceId as! String,
                                           "userId"                                     : userId as! String,
                                           "deviceType"                                 : deviceType]
        
        let urlParams : [String : String] = ["PracticeId"             :   practiceId as! String,
                                             "RequestType"            :   requestType,
                                             "PatientId"              :   patientId,
                                             ]
        
        
        let _ = requestManager.JsonGetRequest(withURL: urlString!, urlParameters: urlParams, headers: headers) { [weak self] (jsonData, error, responceCode) in
            
            DispatchQueue.main.async(execute: {
                
                print("jsonData :- \(String(describing: jsonData))")
                
                if error != nil
                {
                    completion(false, [],[],[],[],[], "Invalid response received from server")
                    return
                }
                
                if responceCode == Constants.ValidResponseCode
                {
                    if let json = jsonData
                    {
                        let prescriptionJsonList = json ["Prescription"].jsonArray
                        let allergyJsonList = json ["Allergies"].jsonArray
                        let medicationJsonList = json ["Medication"].jsonArray
                        let problemJsonList = json ["Problems"].jsonArray
                        let notificationJsonList = json ["Notification"].jsonArray

                        let prescriptionData = Prescription.ArrayFromJsonFactroy(prescriptionJsonList)
                        let allergyData = Allergy.ArrayFromJsonFactroy(allergyJsonList)
                        let medicationData = Medication.ArrayFromJsonFactroy(medicationJsonList)
                        let ProblemData = Problem.ArrayFromJsonFactroy(problemJsonList)
                        let notificationData = Notification.ArrayFromJsonFactroy(notificationJsonList)
                        
                        completion(true, prescriptionData,allergyData,medicationData,ProblemData,notificationData, "")
                    }
                }
                else
                {
                    completion(true, [],[],[],[],[], "Invalid response received from server")
                }
            })
        }
    }
}
